# M-ini.me API Documentation

![logo](https://gitlab.com/eskiasy/m-ini.me-api-documentation-v1/-/raw/master/m%20(3).png?ref_type=heads)

Welcome to the M-ini.me, Free URL Shortener API documentation, the ultimate tool for shortening and tracking your URLs!

## Table of Contents
[[_TOC_]]



---

# 1 Register a New User


## 1.1 Endpoint

- **URL:** `/api/v1/auth/register/`
- **Method:** `POST`

## 1.2 Description

This endpoint enables the registration of a new user. It returns a JSON response with a "status" key indicating the outcome of the request.

## 1.3 Request Parameters

| Parameter   | Type    | Description                                     |
|-------------|---------|-------------------------------------------------|
| `username`  | String  | Unique username for the new user.               |
| `email`     | String  | Valid email address for the new user.           |
| `password`  | String  | Password for the new user.                      |
| `password2` | String  | Confirmatory password for the new user.         |
| `first_name`| String  | First name of the new user.                     |
| `last_name` | String  | Last name of the new user.                      |

### 1.3.1 Parameter Details

- `username`: Alphanumeric, 3-30 characters.
- `email`: Valid email address.
- `password`: At least 8 characters, including a mix of letters, numbers, and special characters.
- `password2`: Must match the password field.
- `first_name`: First name of the user.
- `last_name`: Last name of the user.

## 1.4 Example Request

### Curl Example

```bash
curl -X POST -H "Content-Type: application/json" -d '{
  "username": "john_doe",
  "email": "john.doe@example.com",
  "password": "SecurePassword123",
  "password2": "SecurePassword123",
  "first_name": "John",
  "last_name": "Doe"
}' https://m-ini.me/api/v1/auth/register/
```
### Python Example

```python
import requests
url = "https://m-ini.me/api/v1/auth/register/"
payload = {
  "username": "john_doe",
  "email": "john.doe@example.com",
  "password": "SecurePassword123",
  "password2": "SecurePassword123",
  "first_name": "John",
  "last_name": "Doe"
}
response = requests.post(url, json=payload)
print(response.json())
```

## 1.5 Responses

### 1.5.1 Success Response
- **Status Code**: 201 CREATED
- **Body**: Upon successful registration.
```json
{
  "status": "Success",
  "message": "User Created Successfully!",
  "details": {
    "username": "john_doe",
    "email": "john.doe@example.com",
    "first_name": "John",
    "last_name": "Doe"
  }
}
```
### 1.5.2 Error Responses
- **Status Code**: 400 BAD REQUEST
- **Body**: If the request parameters are invalid or missing.
```json
{
  "status": "Error",
  "message": "Password Error",
  "details": "The provided passwords do not match.",
  "status_code": 400,
  "additional_help": [
    "Ensure that the passwords match and try again.",
    "Consider the following password requirements:",
    "- Your password can’t be too similar to your other personal information.",
    "- Your password must contain at least 8 characters.",
    "- Avoid using commonly used passwords.",
    "- Ensure your password is not entirely numeric."
  ]
}

```

## 1.6 Authentication and Permissions

- **Authentication:** None (Open for all users)
- **Permissions:** None (Open for all users)
---

# 2 Obtain Token Authentication


## 2.1 Endpoint

- **URL:** `/api/v1/auth/token/`
- **Method:** `POST`

## 2.2 Description

This endpoint is used to obtain an API token by providing valid authentication credentials. The token can be used for subsequent authenticated requests.

## 2.3 Request Parameters

| Parameter   | Type    | Description                                     |
|-------------|---------|-------------------------------------------------|
| `username`  | String  | Username for authentication.                    |
| `password`  | String  | Password for authentication.                    |

### 2.3.1 Parameter Details

- `username`: Alphanumeric, 3-30 characters.
- `password`: At least 8 characters, including a mix of letters, numbers, and special characters.

## 2.4 Example Request

### Curl Example

```bash
curl -X POST -H "Content-Type: application/json" -d '{
  "username": "john_doe",
  "password": "SecurePassword123"
}' https://m-ini.me/api/v1/auth/token/
```
### Python Example

```python
import requests
url = "https://m-ini.me/api/v1/auth/token/"
payload = {
  "username": "john_doe",
  "password": "SecurePassword123"
}
headers = {
  "Content-Type": "application/json"
}
response = requests.post(url, json=payload, headers=headers)
print(response.json())
```

## 2.5 Responses

### 2.5.1 Success Response
- **Status Code**: 201 CREATED
- **Body**: Upon successful Authentication.
```json
{
  "token": "your_api_token_here"
}
```
### 2.5.2 Error Responses
- **Status Code**: 400 BAD REQUEST
- **Body**: If the request parameters are invalid or missing.
```json
{
    "username": [
        "This field is required."
    ],
    "status_code": 400
}
```
### 2.5.3 Error Responses
- **Status Code**: 401 UNAUTHORIZED
- **Body**: If the provided credentials are incorrect.
```json
{
   "non_field_errors": [
     "Unable to log in with provided credentials."
   ],
   "status_code": 401
}
```

## 2.6 Authentication and Permissions

- **Authentication:** None (Open for all users)
- **Permissions:** None (Open for all users)

---

# 3 Shorten a URL


## 3.1 Endpoint

- **URL:** `/api/v1/shorten/`
- **Method:** `POST`

## 3.2 Description

This endpoint allows authenticated users to shorten a provided URL. The API token must be included in the request header for authentication.

## 3.3 Request Parameters

| Parameter | Type   | Description                               |
|-----------|--------|-------------------------------------------|
| `url`     | String | The URL to be shortened.                   |

### 3.3.1 Parameter Details

- `url`: The full URL that you want to shorten.

## 3.4 Example Request

### Curl Example

```bash
curl -X POST -H "Content-Type: application/json" -H "Authorization: Token your_api_token_here" -d '{
  "url": "https://example.com/your-long-url"
}' https://m-ini.me/api/v1/shorten/
```
### Python Example

```python
import requests
url = "https://m-ini.me/api/v1/shorten/"
payload = {
  "url": "https://example.com/your-long-url"
}
headers = {
  "Content-Type": "application/json",
  "Authorization": "Token your_api_token_here"
}
response = requests.post(url, json=payload, headers=headers)
print(response.json())

```

## 3.5 Responses
### 3.5.1 Success Response
- **Status Code**: 201 CREATED
- **Body**: Upon successful URL shortening.
```json
{
  "status": "Success",
  "message": "URL shortened Successfully!",
  "status_code": 200,
  "original_url": "https://example.com/your-long-url",
  "short_url": "https://m-ini.me/short-code",
  "url_id": "short-code",
  "name": "site_name"
}
```
### 3.5.2 Error Responses
- **Status Code**: 400 BAD REQUEST
- **Body**: If the request parameters are invalid or missing.
```json
{
    "status": "Error",
    "status_code": 400,
    "message": {
        "url": [
            "This field is required."
        ]
    }
}
```
- **Status Code**: 400 BAD REQUEST
- **Body**: If the request parameters are invalid or missing.
```json
{
  "status": "Error",
  "status_code": 400,
  "message": [
    "URL Structure Integrity Check Failed! The URL you just tried to shorten did not conform to the expected format and/or encountered an issue while checking its integrity. Please ensure the URL is correctly formatted and try again with a more secure URL structure."
  ],
  "original_url": "https://example.com/your-long-url"
}
```
- **Status Code**: 400 BAD REQUEST
- **Body**: If the URL fails security checks.
```json
{
  "status": "Error",
  "status_code": 400,
  "message": [
    "Django Security Validation Check Failed! The URL you just tried to shorten is not valid. Please make sure it follows the correct format (e.g., https://www.example.com) and try again.",
    "Google Safe Browsing Security Check Failed! The URL you just tried to shorten was flagged as suspicious and/or malicious by Google Safe Browsing. Please use a more secure URL in the future.",
    "URLscan.io Security Check Failed! The URL you just tried to shorten was flagged as suspicious and/or malicious by URLscan.io. Please use a more secure URL in the future.",
    "URLscan.io Security check failed unexpectedly. Please try after a few minutes as we need to validate your URL for all security checks prior to shortening."
  ],
  "original_url": "https://example.com/your-long-url"
}
```
- **Status Code**: 400 BAD REQUEST
- **Body**:  If the URL fails Django security checks.
```json
{
  "status": "Error",
  "status_code": 400,
  "message": [
    "Django Security Validation Check Failed! The URL you just tried to shorten did not conform to the expected format. Please use a more secure URL in the future."
  ],
  "original_url": "https://example.com/your-long-url"
}
```
- **Status Code**: 401 UNAUTHORIZED
- **Body**: If the provided API token is incorrect.
```json
{
    "error": "You are not authenticated to perform this action.",
    "status_code": 401,
    "Help": "Please go to /api/v1/register/ to register for a free account and then go to /api/v1/token/ to get your authentication token... If you need more help check out the Documentation at github."
}
```
- **Status Code**: 401 UNAUTHORIZED
- **Body**: If the provided API token is incorrect.
```json
{
    "detail": "Invalid token.",
    "status_code": 401
}
```

## 3.6 Authentication and Permissions

- **Authentication:** Token Authentication (API token must be included in the request header)
- **Permissions:** IsAuthenticated (Only token authenticated users can access)

---

# 4 List Shortened URLs by User


## 4.1 Endpoint

- **URL:** `/api/v1/user/short-urls/`
- **Method:** `GET`

## 4.2 Description

This endpoint allows authenticated users to retrieve a list of their shortened URLs. The API token must be included in the request header for authentication.

## 4.3 Request Parameters

This endpoint does not require any additional request parameters.

## 4.4 Example Request

### Curl Example

```bash
curl -X GET -H "Authorization: Token your_api_token_here" https://m-ini.me/api/v1/user/short-urls/
```

### Python Example
```python
import requests
url = "https://m-ini.me/api/v1/user/short-urls/"
headers = {
  "Authorization": "Token your_api_token_here"
}
response = requests.get(url, headers=headers)
print(response.json())
```

## 4.5 Responses
### 4.5.1 Success Response
- **Status Code**: 201 OK
- **Body**: Upon successfully retrieving the list of shortened URLs.
```json
{
  "count": 21,
  "next": "https://m-ini.me/api/v1/user/short-urls/?page=2",
  "previous": null,
  "results": [
    {
      "name": "site_name_1",
      "url": "https://example.com/your-long-url",
      "short_url": "https://m-ini.me/short-code-1",
      "url_id": "short-code-1",
      "created_on": "2023-12-04T17:28:29.729968Z",
      "counter": 0,
      "has_qr": false,
      "image": null,
      "campaign": null,
      "qr_code": null
    },
    {
      "name": "site_name_2",
      "url": "https://example.com/your-long-url_2",
      "short_url": "https://m-ini.me/short-code-2",
      "url_id": "short-code-2",
      "created_on": "2023-12-05T09:17:37.401954Z",
      "counter": 0,
      "has_qr": false,
      "image": null,
      "campaign": null,
      "qr_code": null
    },
    ...
  ]
}

```
#### Explanation:

- `count`: The total number of shortened URLs available for the authenticated user.
- `next`: The URL to the next page of results. If null, there are no more pages.
- `previous`: The URL to the previous page of results. If null, this is the first page.

### 4.5.2 Error Responses
- **Status Code**: 401 UNAUTHORIZED
- **Body**: If the API token is missing or invalid.
```json
{
  "error": "Please Login or Authenticate with your security Token to Proceed.",
  "status_code": 401
}
```
- **Status Code**: 401 UNAUTHORIZED
- **Body**: If the API token is missing or invalid.
```json
{
  "error": "Invalid token.",
  "status_code": 401
}
```

## 4.6 Authentication and Permissions

- **Authentication:** Token Authentication (API token must be included in the request header)
- **Permissions:** IsAuthenticated (Only token authenticated users can access)



# 5 View Single Short URL Details and Stats by Shortcode
<!-- TODO -->

## 5.1 Endpoint

- **URL:** `/api/v1/user/short-urls/<str:url_id>/`
- **Method:** `GET`

## 5.2 Description

This endpoint allows authenticated users to retrieve detailed information about a shortened URL using its unique shortcode (`url_id`). The API token must be included in the request header for authentication.

## 5.3 Request Parameters

- `url_id` (String): The unique shortcode identifying the shortened URL.

## 5.4 Example Request

### Curl Example

```bash
curl -X GET -H "Authorization: Token your_api_token_here" https://m-ini.me/api/v1/user/short-urls/your_url_id_here/
```
### Python Example

```python
import requests

url_id = "short_url_id_here"
url = f"https://m-ini.me/api/v1/user/short-urls/{url_id}/"

headers = {
  "Authorization": "Token your_api_token_here"
}

response = requests.get(url, headers=headers)
print(response.json())
```
## 5.5 Responses

### 5.5.1 Success Response
- **Status Code**: 201 OK
- **Body**: Upon successful registration.
```json
{
    "name": "site_name",
    "url": "https://example.com/your-long-url",
    "short_url": "https://m-ini.me/short-code",
    "url_id": "short-code",
    "created_on": "2023-12-10T15:26:51.146234Z",
    "counter": 0,
    "has_qr": false,
    "image": null,
    "campaign": null,
    "qr_code": null
}
```
### 5.5.2 Error Responses
- **Status Code**: 404 NOT FOUND
- **Body**: If the specified url_id does not exist or is not associated with the authenticated user.
```json
{
    "error": "API Endpoint / shortURL not found.",
    "status_code": 404,
    "additional_help": "You requested an API endpoint or a short URL code that could not be found. Please check the URL and try again."
}

```
## 5.6 Authentication and Permissions

- **Authentication:** Token Authentication (API token must be included in the request header)
- **Permissions:** IsAuthenticated (Only token authenticated users can access)


# 6 Convert Images to Sharable Short Links [NEW]

## 6.1 Endpoint

- **URL:** `/api/v1/user/img-to-link/`
- **Method:** `POST`

## 6.2 Description

This endpoint allows authenticated users to create a shareable image with a link that is shortened using the M-ini.me URL Shortener API. The API token must be included in the request header for authentication.

![logo](https://gitlab.com/eskiasy/m-ini.me-api-documentation-v1/-/raw/master/img_to_link_example.png?ref_type=heads)


## 6.3 Request Parameters

- `url` (String): The URL to be shortened and associated with the shareable image.
- `image` (File, optional): An optional image file to be associated with the shortened URL.
- `image_og_custom_title` (String, optional): Custom title for the Open Graph metadata of the image. If not provided, it will use the `site name` by default.
- `image_og_custom_description` (String, optional): Custom description for the Open Graph metadata of the image. If not provided, it will use the `site name` by default.
- `image_og_custom_width` (Integer, optional): Custom width for the Open Graph metadata of the image. If not provided, it will use `1200` by default.
- `image_og_custom_height` (Integer, optional): Custom height for the Open Graph metadata of the image. If not provided, it will use `630` by default.
- `image_og_custom_type` (String, optional): Custom type for the Open Graph metadata of the image. Allowed values are `jpeg, png, gif, jpg`. If not provided or if the value deviates from allowed values, it will use `jpg` by default.

> **Note:** If an image is provided, the associated file type should be explicitly provided in the `image_og_custom_type` argument for seamless functionality. The payload structure should be as follows:

```python
image_type = "jpg" # Or your image file type
payload = {
  'url': 'https://example.com/your-long-url-to-redirect-to',
  'image_og_custom_title': 'Custom Title',
  'image_og_custom_description': 'Custom Description',
  'image_og_custom_width': 800, # Your image width
  'image_og_custom_height': 600, # Your image height
  'image_og_custom_type': 'image/{}'.format(image_type),
}
files=[
  ('image',('your_image_file.jpg',open('your_image_file_with_full_path.jpg','rb'), 'image/{}'.format(image_type)))
]
```

## 6.4 Example Request

### Curl Example

```bash
curl -X POST \
  'https://m-ini.me/api/v1/user/img-to-link/' \
  -H 'Authorization: Token your_token_here' \
  -F 'image=@"your_image_file.jpg"' \
  -F 'url="https://example.com/your-long-url"' \
  -F 'image_og_custom_title="Custom Title"' \
  -F 'image_og_custom_description="Custom Description"' \
  -F 'image_og_custom_width=Your image width (eg. 800)' \
  -F 'image_og_custom_height=Your image height (eg. 600)' \
  -F 'image_og_custom_type="image/your_image_file_type"'
```
### Python Example

```python
import requests

url = "https://m-ini.me/api/v1/user/img-to-link/"
image_type = "jpeg" # Or your image file type
payload = {
  'url': 'https://example.com/your-long-url-to-redirect-to',
  'image_og_custom_title': 'Custom Title',
  'image_og_custom_description': 'Custom Description',
  'image_og_custom_width': 800, # Your image width
  'image_og_custom_height': 600, # Your image height
  'image_og_custom_type': 'image/{}'.format(image_type),
}
files=[
  ('image',('your_image_file.jpg',open('your_image_file_with_full_path.jpg','rb'), 'image/{}'.format(image_type)))
]
headers = {
  'Authorization': 'Token your_api_token_here'
}
response = requests.request("POST", url, headers=headers, data=payload, files=files)
print(response.json())
```
## 6.5 Responses

### 6.5.1 Success Response
- **Status Code**: 201 CREATED
- **Body**: Upon successful registration.
```json
{
  "status": "Success",
  "message": "Shareable Image with Link created and shortened Successfully!",
  "status_code": 200,
  "original_url": "https://example.com/your-long-url",
  "short_url": "https://m-ini.me/short-code",
  "url_id": "short-code",
  "name": "site_name"
}
```
### 6.5.2 Error Responses
- **Status Code**: 400 BAD REQUEST
- **Body**: If the request parameters are invalid or missing.
```json
{
    "status": "Error",
    "status_code": 400,
    "message": {
        "url": [
            "This field is required."
        ]
    }
}
```
- **Status Code**: 400 BAD REQUEST
- **Body**: If the request parameters are invalid or missing.
```json
{
  "status": "Error",
  "status_code": 400,
  "message": [
    "URL Structure Integrity Check Failed! The URL you just tried to shorten did not conform to the expected format and/or encountered an issue while checking its integrity. Please ensure the URL is correctly formatted and try again with a more secure URL structure."
  ],
  "original_url": "https://example.com/your-long-url"
}
```
- **Status Code**: 400 BAD REQUEST
- **Body**: If the URL fails security checks.
```json
{
  "status": "Error",
  "status_code": 400,
  "message": [
    "Django Security Validation Check Failed! The URL you just tried to shorten is not valid. Please make sure it follows the correct format (e.g., https://www.example.com) and try again.",
    "Google Safe Browsing Security Check Failed! The URL you just tried to shorten was flagged as suspicious and/or malicious by Google Safe Browsing. Please use a more secure URL in the future.",
    "URLscan.io Security Check Failed! The URL you just tried to shorten was flagged as suspicious and/or malicious by URLscan.io. Please use a more secure URL in the future.",
    "URLscan.io Security check failed unexpectedly. Please try after a few minutes as we need to validate your URL for all security checks prior to shortening."
  ],
  "original_url": "https://example.com/your-long-url"
}
```
- **Status Code**: 400 BAD REQUEST
- **Body**:  If the URL fails Django security checks.
```json
{
  "status": "Error",
  "status_code": 400,
  "message": [
    "Django Security Validation Check Failed! The URL you just tried to shorten did not conform to the expected format. Please use a more secure URL in the future."
  ],
  "original_url": "https://example.com/your-long-url"
}
```
- **Status Code**: 401 UNAUTHORIZED
- **Body**: If the provided API token is incorrect.
```json
{
    "error": "You are not authenticated to perform this action.",
    "status_code": 401,
    "Help": "Please go to /api/v1/register/ to register for a free account and then go to /api/v1/token/ to get your authentication token... If you need more help check out the Documentation at github."
}
```
- **Status Code**: 401 UNAUTHORIZED
- **Body**: If the provided API token is incorrect.
```json
{
    "detail": "Invalid token.",
    "status_code": 401
}
```
## 6.6 Authentication and Permissions

- **Authentication:** Token Authentication (API token must be included in the request header)
- **Permissions:** IsAuthenticated (Only token authenticated users can access)



Thank you for choosing M-ini.me! I am committed to providing you with a seamless URL shortening and tracking experience. Enjoy the benefits of simplified link management, analytics, and more.
