# M-ini.me URL Shortener API Documentation
![Latest commit](https://img.shields.io/gitlab/last-commit/eskiasy/m-ini.me-api-documentation-v1?style=round-square)

![logo](https://gitlab.com/eskiasy/m-ini.me-api-documentation-v1/-/raw/master/m%20(2).png?ref_type=heads)

Welcome to the official API documentation for M-ini.me URL Shortener! This document provides comprehensive information on the available endpoints, request parameters, responses, and examples to help you integrate seamlessly with the M-ini.me URL Shortener API.

## Table of Contents

1. [Introduction](#introduction)
2. [Authentication](#authentication)
3. [Endpoints](#endpoints)
    - [Register New User](#1-register-new-user)
    - [API Token Authentication](#2-api-token-authentication)
    - [Shorten URL](#3-shorten-url)
    - [List Shortened URLs by User](#4-list-shortened-urls-by-user)
    - [View Single Short URL Details and Stats by Shortcode](#5-view-single-short-url-details-and-stats-by-shortcode)
    - [Convert Images to Sharable Short Links [NEW]](#6-convert-images-to-sharable-short-links-new)
4. [Usage Examples](#usage-examples)
5. [API Usage Guidelines](#api-guidelines)
6. [Contributing](#contributing)
7. [License](#license)

## Introduction

Welcome to M-ini.me, the ultimate tool for shortening and tracking your URLs! This free URL shortener comes with basic tracking, analytics, QR code generation, and campaign support. My mission is to provide you with a fast, reliable, and secure way to shorten your links and make them more memorable.

## Authentication

To use the M-ini.me URL Shortener API, you need to authenticate with your API token. You can obtain the token by registering as a new user or using your existing login credentials for m-ini.me web and then using the obtained token for subsequent authenticated requests.

## Endpoints

### 1. Register New User

- **Endpoint:** `/api/v1/auth/register/`
- **Method:** `POST`
- **Description:** Register a new user with the provided information.
- **[Full Documentation](https://gitlab.com/eskiasy/m-ini.me-api-documentation-v1/-/blob/master/M-ini.me%20API%20Documentation%20(V1-draft_2)%5BJB%5D.md?ref_type=heads#1-register-a-new-user)**

### 2. API Token Authentication

- **Endpoint:** `/api/v1/auth/token/`
- **Method:** `POST`
- **Description:** Obtain an API token for authentication.
- **[Full Documentation](https://gitlab.com/eskiasy/m-ini.me-api-documentation-v1/-/blob/master/M-ini.me%20API%20Documentation%20(V1-draft_2)%5BJB%5D.md?ref_type=heads#2-obtain-token-authentication)**

### 3. Shorten URL

- **Endpoint:** `/api/v1/shorten/`
- **Method:** `POST`
- **Description:** Shorten a URL and retrieve details.
- **[Full Documentation](https://gitlab.com/eskiasy/m-ini.me-api-documentation-v1/-/blob/master/M-ini.me%20API%20Documentation%20(V1-draft_2)%5BJB%5D.md?ref_type=heads#3-shorten-a-url)**

### 4. List Shortened URLs by User

- **Endpoint:** `/api/v1/user/short-urls/`
- **Method:** `GET`
- **Description:** List all shortened URLs by the authenticated user.
- **[Full Documentation](https://gitlab.com/eskiasy/m-ini.me-api-documentation-v1/-/blob/master/M-ini.me%20API%20Documentation%20(V1-draft_2)%5BJB%5D.md?ref_type=heads#4-list-shortened-urls-by-user)**

### 5. View Single Short URL Details and Stats by Shortcode

- **Endpoint:** `/api/v1/user/short-urls/<str:url_id>/`
- **Method:** `GET`
- **Description:** List all shortened URLs by the authenticated user.
- **[Full Documentation](https://gitlab.com/eskiasy/m-ini.me-api-documentation-v1/-/blob/master/M-ini.me%20API%20Documentation%20(V1-draft_2)%5BJB%5D.md?ref_type=heads#5-view-single-short-url-details-and-stats-by-shortcode)**

### 6. Convert Images to Sharable Short Links [NEW]

- **Endpoint:** `/api/v1/user/img-to-link/`
- **Method:** `POST`
- **Description:** List all shortened URLs by the authenticated user.
- **[Full Documentation](https://gitlab.com/eskiasy/m-ini.me-api-documentation-v1/-/blob/master/M-ini.me%20API%20Documentation%20(V1-draft_2)%5BJB%5D.md?ref_type=heads#6-convert-images-to-sharable-short-links-new)**


## Usage Examples

To demonstrate how to interact with the API, I provide examples in both cURL and Python. Follow the examples to understand how to make requests and handle responses effectively.


## API Usage Guidelines and Rate Limits

### Introduction

M-ini.me API is designed to provide a fast and reliable URL shortening service along with additional features. To ensure the stability and fair usage of the API, please adhere to the following guidelines and be aware of rate limits.

### Authentication

All requests to the API must include a valid API token in the Authorization header. You can obtain an API token by registering on the M-ini.me website and using the `/api/v1/auth/token/` endpoint.

### Example:

```bash
curl -X POST -H "Authorization: Token your_api_token_here" https://m-ini.me/api/v1/user/short-urls/
```

### Rate Limits

To prevent abuse and ensure fair usage, the API enforces rate limits. The rate limits are as follows:

**Anonymous Users:** No Access To API.

**Authenticated Users:** **4/m (short links)** and **2/m (image-to-url-links)** are provided for authenticated users for the time being.

### Guidelines for Efficient Use

**Batch Requests:** Whenever possible, use batch requests to shorten multiple URLs in a single request to reduce the number of API calls.

**Error Handling:** Handle errors gracefully and avoid making repeated requests if the same error persists.


## Contributing

I welcome contributions from the community! If you find a bug, have a feature request, or want to contribute to the documentation, please submit an issue or pull request. Your input is valuable, and I appreciate your support.

## Donate a coffee

<a href="https://www.buymeacoffee.com/eskiasyilma" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" alt="Buy Me A Coffee" style="height: 41px !important;width: 174px !important;box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;" ></a>

## Authors

- [Eskias Yilma](https://gitlab.com/eskiasy)
   - [Twitter](https://twitter.com/eskiasyilma)
   - [Email](eskiasy@gmail.com)



# _Happy Shortening with M-ini.me!_
